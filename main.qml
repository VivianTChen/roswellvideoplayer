import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Window 2.2
import QtQuick.Controls.Material 2.4

ApplicationWindow {
    visible: true
    width: 800
    height: 480
    title: qsTr("Roswell Video Player")
    flags: Qt.FramelessWindowHint

    readonly property var fontAwesome: FontLoader {
        name: "fontawesome"
        source: "qrc:/fa-solid-900.ttf"
    }

    property color primaryColor: Material.primary

    Rectangle {
        anchors.fill: parent
        visible: (Qt.platform.os === "windows" )
        Image {
            id: bgd
            anchors.fill: parent
            width: parent.width
            height: parent.height
            fillMode: Image.PreserveAspectCrop
            asynchronous: true
            source: "image1.jpg"
        }
    }

    VideoPlayerPage {
        anchors.fill: parent
        //enabled: (Qt.platform.os !== "windows" )
    }

}
