import QtQuick 2.0

Rectangle {
    id: root

// public
    property string text: 'text'
    property string bcolor: "white"
    property real size: 50

    signal clicked();

// private
    width: size
    height: size
    color: "transparent"
    border.color: root.bcolor
    border.width: 1
    radius:       root.height / 2
    opacity:      enabled  &&  !mouseArea.pressed? 1: 0.3

    Text {
        anchors.centerIn: parent
        text: root.text
        font.pixelSize: parent.height * 0.6
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        color: root.bcolor
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked:  root.clicked()
    }
}
