import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtMultimedia 5.13
import QtQuick.Controls.Material 2.4

Item {
    id: videoPlayerPage

    property string name: "VideoPlayerPage"

    VideoOutput {
        id: video
        anchors.fill: parent
        source: mediaPlayer
        flushMode: VideoOutput.LastFrame

        property alias mediaSource: mediaPlayer.source
        property alias volume: mediaPlayer.volume
        property bool isRunning: true

        MediaPlayer {
            id: mediaPlayer
            autoPlay: true

            //source: "file://" + videoFile

            playlist: Playlist {
                id: playlist
                //PlaylistItem { source: "file:///home/georges/Videos/BWC/SK.mp4"; }
                Component.onCompleted: {
                    if (videoFileParam)
                        playlist.addItems(videoFileParam)
                    //else
                        //playlist.addItem("file:///home/georges/Videos/BWC/SK.mp4")
                    video.toggleplay()
                }
            }
            volume: volumeSlider.volume

        }

        /*function play() { mediaPlayer.play() }
        function pause() { mediaPlayer.pause() }
        function stop() { mediaPlayer.stop() }*/

        function toggleplay() {
            if(mediaPlayer.playbackState != MediaPlayer.PlayingState)
                mediaPlayer.play()
            else
                mediaPlayer.pause()
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                video.toggleplay()
            }
        }
    }


    Rectangle {
        id: topBar
        visible: false
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        height: 50
        color: primaryColor
        opacity: 0.5
    }

    Rectangle {
        id: exitIcon
        anchors.top: parent.top
        anchors.right: parent.right
        height: 48
        width: height
        color: "transparent"
        radius: height/8

        Text {
            anchors.fill: parent
            text: "\uf00d"
            font.pixelSize: 0.7 * parent.height
            font.family: "Font Awesome 5 Free"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: "white"
        }

        MouseArea {
            id: exitMouseArea
            anchors.fill: parent
            onClicked: Qt.quit()
        }
    }

    Rectangle {
        id: controlBar
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        height: 50
        color: primaryColor
        opacity: 0.5

        RowLayout {
            
            anchors.fill: parent

            Rectangle {
                id: playIcon
                Layout.leftMargin: 10
                height: parent.height-2
                width: height
                border.width: 1
                radius: height/8

                Text {

                    anchors.fill: parent
                    //text: playIcon.checked? "\u25b6" : "\u2551" //using unicode charactors
                    text: mediaPlayer.playbackState == MediaPlayer.PlayingState ? "\uf7a5" : "\uf04b"
                    font.pixelSize: 0.7 * parent.height
                    font.family: "Font Awesome 5 Free"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }

                MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    onClicked: {
                        video.toggleplay()
                    }
                }
            }

            Rectangle {
                id: sliderBar
                Layout.fillWidth: true
                Label {
                    id: positionLabel
                    anchors.right: seekSlider.left
                    anchors.rightMargin: 10
                    height: parent.height
                    text: mediaPlayer.position
                    verticalAlignment:Text.AlignVCenter
                }

                Slider {
                    id: seekSlider
                    anchors.centerIn: parent
                    width: parent.width * 0.5
                    value: mediaPlayer.position // for slider to move along with movie
                    from: 0
                    to: (Qt.platform.os === "windows" )? 10 : mediaPlayer.duration

                    onPressedChanged: {
                        mediaPlayer.seek(seekSlider.value)
                    }
                }

                Label {
                    id: durationLabel
                    anchors.left: seekSlider.right
                    anchors.leftMargin: 10
                    height: parent.height
                    text: seekSlider.to //mediaPlayer.duration
                    verticalAlignment:Text.AlignVCenter
                }


            }

            Rectangle {
                id: volumnBar
                width: 100

                Label {
                    anchors.right: volumeSlider.left
                    anchors.rightMargin: 10
                    height: parent.height
                    text: "\uf028"
                    font.pixelSize: 0.4 * controlBar.height
                    font.family: "Font Awesome 5 Free"
                    verticalAlignment:Text.AlignVCenter
                }

                Slider {
                    id: volumeSlider
                    anchors.right: parent.right
                    anchors.rightMargin: 10
                    anchors.verticalCenter: parent.verticalCenter
                    value: 0.5
                    width: 100

                    property real volume: QtMultimedia.convertVolume(volumeSlider.value,
                                                                     QtMultimedia.LogarithmicVolumeScale,
                                                                     QtMultimedia.LinearVolumeScale)
                }

            }
        }

    }

    RoundButton1 {
        id: videoPrev
        visible: false
        anchors {
            top: parent.top
            topMargin: parent.height/2 - height/2
            left: parent.left
            leftMargin: height/2
        }

        text: "<"
        //bcolor: primaryColor
        onClicked: {

        }
    }

    RoundButton1 {
        id: videoNext
        visible: false
        anchors {
            top: parent.top
            topMargin: parent.height/2 - height/2
            right: parent.right
            rightMargin: height/2
        }

        text: ">"
        //bcolor: primaryColor
        onClicked: {

        }
    }

    RoundButton1 {
        id: playInd
        size: 100
        anchors {
            top: parent.top
            topMargin: parent.height/2 - height/2
            left: parent.left
            leftMargin: parent.width/2 - width/2
        }
        text: " \u25b6"
        //bcolor: primaryColor
        opacity: 0.6
        visible: mediaPlayer.playbackState == MediaPlayer.PlayingState ? false : true
        onClicked: {
            video.toggleplay()
        }
    }

    Timer  {
            id: topBarShowimer
            interval: 10000;
            running: true;
            repeat: false;
            onTriggered: {
                topBar.visible = true
                videoPrev.visible = true
                videoNext.visible = true
            }
        }

    // emitting a Signal could be another option
    Component.onDestruction: {
        cleanup()
    }


    // called immediately after Loader.loaded
    function init() {
        console.log("Init done from VideoPlayerPage")
    }
    // called from Component.destruction
    function cleanup() {
        console.log("Cleanup done from VideoPlayerPage")
    }
}
