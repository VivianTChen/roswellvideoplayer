#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    QList<QUrl> sources;

    //set input video file param
    if (argc > 1 ) {
        QString videoFileParam =  QString::fromLocal8Bit(argv[1]);

        qDebug() << "pass parameters: " << videoFileParam;
        sources << QUrl::fromLocalFile(videoFileParam);
        engine.rootContext()->setContextProperty("videoFileParam", QVariant::fromValue(sources));
      }
    else {
        engine.rootContext()->setContextProperty("videoFileParam", nullptr);
    }
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
